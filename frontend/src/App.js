import React, { useState } from "react";
import "./App.css";

const API = "http://095dd1d5.ngrok.io";

async function apiRoll(number) {
  const r = await fetch(`${API}/roll`, {
    method: "POST"
  }).then(res => res.json());
}

function App() {
  const [number, setNumber] = useState("");
  const [loading, setLoading] = useState(false);

  const onInput = ({ target }) => {
    setNumber(target.value);
  };

  const onSubmit = async event => {
    event.preventDefault();

    if (!number) return;

    setLoading(true);
    await apiRoll(number);
    setLoading(false);
    setNumber("");
  };

  return (
    <div className="App">
      <header className="App-header">header</header>

      <form onSubmit={onSubmit}>
        <label>
          Victims phone number:
          <input value={number} onChange={onInput} type="tel" />
        </label>

        <button type="submit" disabled={loading}>
          Send{loading ? "ing..." : ""}
        </button>
      </form>
    </div>
  );
}

export default App;
