const twilio = require('twilio')

const accountSid = process.env.ACCOUNT_SID;
const accountToken = process.env.ACCOUNT_TOKEN;
const from = process.env.FROM_NUMBER;

const client = twilio(accountSid, accountToken);

exports.twilio = async (to, body) => {
  const message = await client.messages.create({
    body,
    from,
    to
  })
  console.log(message)
  return message 
}

exports.text = async (number) => {
  await new Promise(res => setTimeout(res, 500))
  console.log(number)
}
