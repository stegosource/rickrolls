module.exports = [
  {
    message: "Wubba lubba dub dub!",
    season: 1,
    episode: 1,
    character: "Rick Sanchez"
  },
  {
    message: "Pickle Rick!!!",
    season: 2,
    episode: 3,
    character: "Rick Sanchez"
  },
  {
    message: "Sometimes science is a lot more art than science. A lot of people don't get that.",
    season: 4,
    episode: 5,
    character: "Rick Sanchez"
  },
  {
    message: "We're gonna die, Morty!",
    season: 5,
    episode: 6,
    character: "Rick Sanchez"
  },
  {
    message:
      "Nobody exists on purpose. Nobody belongs anywhere. Everybody's gonna die. Come watch TV?",
    season: 7,
    episode: 8,
    character: "Rick Sanchez"
  },
  {
    message: "There's no God, Summer. Gotta rip that bandaid off now. You'll thank me later.",
    season: 1,
    episode: 1,
    character: "Rick Sanches"
  },
  {
    message: "You realize night time makes up half of all time?",
    season: 1,
    episode: 1,
    character: "Rick Sanches"
  }
];
