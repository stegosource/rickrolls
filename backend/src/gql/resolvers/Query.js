const quotes = require('../../data/quotes')
const { twilio } = require('../../services/twilio')

function randomQuote() {
  const index = Math.floor(Math.random() * quotes.length)
  return quotes[index] 
}

const Query = {
  quotes: (_, params) => {
    return randomQuote()
  },

  rickRollz: async (_, { number }) => {
    const quote = randomQuote()
    await twilio(+number, quote.message)
    return quote
  }

  // twilio from number: 17813949142
}

module.exports = Query

