# Rick Rolls

A service to randomly receive motiviational quotes from Rick Sanchez.

> Wubba lubba dub dub!

# Development

## Backend

`npm start`

## Frontend

## Query Example

```
query {
  rickRollz(number: "18008675309") {
    message
    character
    season
    episode
  }
}
```
